EESchema Schematic File Version 4
LIBS:RAMPS-1.6-cache
EELAYER 26 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 1
Title "BQ105-A4-SchDoc"
Date "17 02 2019"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 2550 5035 0    200  ~ 40
POWER
Text Notes 9350 5085 0    200  ~ 40
Servos
Text Notes 7300 5085 0    200  ~ 40
Reset
Text Notes 11650 4985 0    200  ~ 40
AUX-1
Text Notes 13650 4985 0    200  ~ 40
AUX-2
Text Notes 15650 4935 0    200  ~ 40
AUX-3
Text Notes 17500 5035 0    200  ~ 40
AUX-4
Text Notes 3200 9635 0    432  ~ 86
X
Text Notes 8600 9685 0    432  ~ 86
Y
Wire Notes Line
	21950 5485 400  5485
Wire Notes Line
	19750 1135 19750 5485
Wire Notes Line
	17250 1135 17250 5485
Wire Notes Line
	15150 1135 15150 5485
Wire Notes Line
	13300 1135 13300 5485
Wire Notes Line
	11400 1135 11400 5485
Wire Notes Line
	9100 1135 9100 5485
Wire Notes Line
	7000 1135 7000 5485
Text Notes 13940 9635 0    432  ~ 86
Z
Text Notes 19460 9685 0    432  ~ 86
E0
Text Notes 19450 13635 0    432  ~ 86
E1
Wire Notes Line
	21950 9735 300  9735
Wire Notes Line
	5900 5485 5900 9685
Wire Notes Line
	11200 9685 11200 5485
Wire Notes Line
	16950 9685 16950 5485
Wire Notes Line
	16980 13815 16950 9785
Wire Notes Line
	22050 13785 16950 13785
Wire Notes Line
	5900 16185 5900 9735
Wire Notes Line
	11200 16235 11200 9735
Wire Notes Line
	11200 12435 6000 12435
Text Notes 7850 10185 0    252  ~ 50
End  stops
Text Notes 1950 10385 0    252  ~ 50
Heaters &  Fans
Wire Notes Line
	5900 14535 300  14535
Text Notes 3200 16035 0    252  ~ 50
I2C
Text Notes 20050 5250 0    200  ~ 40
to Arduino Mega
Wire Notes Line
	13250 16185 13250 9785
Text Notes 13350 12800 0    75   ~ 0
(raw Google Translate output)\nBQ105-A3 Description:\n1) Change the original power supply 4P socket to 2P socket.\n2) Change the original two-layer board to four-layer board.\n3) Change the hot bed power terminal.\n4) Increase the power of the hot zone of the strip.
Text Notes 13350 13900 0    75   ~ 0
(raw Google Translate output)\nBQ105-A4 fitting instructions:\n1) Trial components OK!\n2) can bring hot bed maximum power: 170W\n3) There is no open circuit or short circuit.\n4) X, Y, Z, E0 motors are OK!\n5) Actual print test OK!
Text Notes 2550 13200 0    96   ~ 19
Bed output
Text Notes 2450 11985 0    90   ~ 18
Fan output\n(with polarity)
Text Notes 1950 10900 0    96   ~ 19
Hotend output
Text Notes 900  2750 0    90   ~ 18
+12V
Text Notes 1100 2850 0    90   ~ 18
-
Text Notes 8200 13400 3    72   ~ 14
Hotend
Text Notes 8470 13385 3    72   ~ 14
Bed
Text Notes 7640 12205 0    132  ~ 26
(microswitch to pins 1 & 2)
Wire Notes Line
	18280 16205 18280 13795
$Comp
L Device:Fuse F1
U 1 1 5C691CF7
P 2150 2250
F 0 "F1" V 2250 2300 60  0000 R BNN
F 1 "30A (16V/30A)" V 2100 2650 60  0000 R BNN
F 2 "Fuse:Fuse_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2740 2460 60  0001 C CNN
F 3 "" H 2740 2460 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    2150 2250
	0    1    -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J11
U 1 1 5C691CF6
P 10700 2700
F 0 "J11" H 10850 2550 60  0000 L BNN
F 1 "SER3" H 10850 2650 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 10850 2400 60  0001 C CNN
F 3 "" H 10850 2400 60  0000 C CNN
	1    10700 2700
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J13
U 1 1 5C691CF5
P 10700 3350
F 0 "J13" H 10850 3200 60  0000 L BNN
F 1 "SER4" H 10850 3300 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 10850 3050 60  0001 C CNN
F 3 "" H 10850 3050 60  0000 C CNN
	1    10700 3350
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 5C691CF4
P 10700 1650
F 0 "J3" H 10850 1500 60  0000 L BNN
F 1 "SER1" H 10850 1600 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 10850 1350 60  0001 C CNN
F 3 "" H 10850 1350 60  0000 C CNN
	1    10700 1650
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J7
U 1 1 5C691CF3
P 10700 2200
F 0 "J7" H 10850 2050 60  0000 L BNN
F 1 "SER2" H 10850 2150 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 10850 1900 60  0001 C CNN
F 3 "" H 10850 1900 60  0000 C CNN
	1    10700 2200
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J12
U 1 1 5C691CF2
P 12750 3250
F 0 "J12" H 12700 3450 60  0000 L BNN
F 1 "A-OUT" H 12650 2900 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 12650 2600 60  0001 C CNN
F 3 "" H 12650 2600 60  0000 C CNN
	1    12750 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J6
U 1 1 5C691CF1
P 12750 2200
F 0 "J6" H 12700 2400 60  0000 L BNN
F 1 "SERIAL" H 12600 1850 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 12650 1550 60  0001 C CNN
F 3 "" H 12650 1550 60  0000 C CNN
	1    12750 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J8
U 1 1 5C691CF0
P 14150 3050
F 0 "J8" H 14150 3350 60  0000 L BNN
F 1 "AUX-2" H 14050 2700 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 14100 1850 60  0001 C CNN
F 3 "" H 14100 1850 60  0000 C CNN
	1    14150 3050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J9
U 1 1 5C691CEF
P 16100 2750
F 0 "J9" H 16100 2950 60  0000 L BNN
F 1 "AUX-3" H 16000 2400 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 16050 1750 60  0001 C CNN
F 3 "" H 16050 1750 60  0000 C CNN
	1    16100 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x18 J2
U 1 1 5C691CEE
P 18600 2800
F 0 "J2" H 18550 1800 60  0000 L TNN
F 1 "AUX-4" H 18450 3750 60  0000 L TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x18_P2.54mm_Vertical" H 18500 2950 60  0001 C CNN
F 3 "" H 18500 2950 60  0000 C CNN
	1    18600 2800
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x18_Odd_Even J1
U 1 1 5C691CED
P 21350 2850
F 0 "J1" H 21350 1750 60  0000 L BNN
F 1 "2x18" H 21300 3850 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x18_P2.54mm_Vertical" H 21300 -1050 60  0001 C CNN
F 3 "" H 21300 -1050 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    21350 2850
	-1   0    0    1   
$EndComp
$Comp
L Device:Fuse F2
U 1 1 5C691CEC
P 2500 2700
F 0 "F2" V 2595 2760 60  0000 R BNN
F 1 "MFR500 (30V  5A)" V 2445 3110 60  0000 R BNN
F 2 "Fuse:Fuse_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3140 2410 60  0001 C CNN
F 3 "" H 3140 2410 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    2500 2700
	0    1    -1   0   
$EndComp
$Comp
L Device:R_US R11
U 1 1 5C691CEB
P 2700 11100
F 0 "R11" V 2650 11050 60  0000 L BNN
F 1 "10" V 2800 11050 60  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2700 10960 60  0001 C CNN
F 3 "" H 2700 10960 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    2700 11100
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R13
U 1 1 5C691CEA
P 2700 11350
F 0 "R13" V 2650 11300 60  0000 L BNN
F 1 "100K" V 2800 11250 60  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2680 11210 60  0001 C CNN
F 3 "" H 2680 11210 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    2700 11350
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NMOS_GDS Q1
U 1 1 5C691CE9
P 3400 11100
F 0 "Q1" H 3100 11270 60  0000 L BNN
F 1 "STP55NF06" H 2850 11120 60  0000 L BNN
F 2 "" H 2850 11120 60  0001 C CNN
F 3 "" H 2850 11120 60  0000 C CNN
	1    3400 11100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R15
U 1 1 5C691CE7
P 2650 12300
F 0 "R15" V 2585 12260 60  0000 L BNN
F 1 "10" V 2785 12260 60  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2650 12160 60  0001 C CNN
F 3 "" H 2650 12160 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    2650 12300
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R17
U 1 1 5C691CE6
P 2650 12550
F 0 "R17" V 2615 12520 60  0000 L BNN
F 1 "100K" V 2765 12470 60  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2630 12410 60  0001 C CNN
F 3 "" H 2630 12410 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    2650 12550
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NMOS_GDS Q2
U 1 1 5C691CE5
P 3400 12300
F 0 "Q2" H 3200 12470 60  0000 L BNN
F 1 "STP55NF06" H 2850 12350 60  0000 L BNN
F 2 "" H 2900 12320 60  0001 C CNN
F 3 "" H 2900 12320 60  0000 C CNN
	1    3400 12300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R19
U 1 1 5C691CE4
P 2700 13450
F 0 "R19" V 2650 13400 60  0000 L BNN
F 1 "10" V 2800 13450 60  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2700 13310 60  0001 C CNN
F 3 "" H 2700 13310 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    2700 13450
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R23
U 1 1 5C691CE3
P 2700 13750
F 0 "R23" V 2615 13670 60  0000 L BNN
F 1 "100K" V 2815 13670 60  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2680 13610 60  0001 C CNN
F 3 "" H 2680 13610 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    2700 13750
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NMOS_GDS Q3
U 1 1 5C691CE2
P 3400 13450
F 0 "Q3" H 3700 13270 60  0000 L BNN
F 1 "WSK220N04" H 3600 13120 60  0000 L BNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 3600 13120 60  0001 C CNN
F 3 "" H 3600 13120 60  0000 C CNN
	1    3400 13450
	1    0    0    -1  
$EndComp
$Comp
L Device:D_ALT D2
U 1 1 5C691CE1
P 3800 3000
F 0 "D2" V 3910 2920 60  0000 R TNN
F 1 "1N4004 (M4)" V 3800 2950 60  0000 R TNN
F 2 "Diode_SMD:D_SMA" H 3800 2950 60  0001 C CNN
F 3 "" H 3800 2950 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    3800 3000
	0    1    1    0   
$EndComp
$Comp
L Device:LED_ALT D3
U 1 1 5C691CE0
P 4200 11450
F 0 "D3" V 4200 11350 60  0000 R TNN
F 1 "~" H 4200 11450 50  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4200 11450 50  0001 C CNN
F 3 "" H 4200 11450 50  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    4200 11450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R16
U 1 1 5C691CDF
P 3850 11100
F 0 "R16" H 4100 11200 60  0000 R TNN
F 1 "1.8K" H 4100 11100 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3810 11035 60  0001 C CNN
F 3 "" H 3810 11035 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    3850 11100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R18
U 1 1 5C691CDD
P 4200 12800
F 0 "R18" V 4250 12900 60  0000 R TNN
F 1 "1.8K" V 4050 12900 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4160 12735 60  0001 C CNN
F 3 "" H 4160 12735 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    4200 12800
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED_ALT D6
U 1 1 5C691CDC
P 3850 13000
F 0 "D6" V 3850 12900 60  0000 R TNN
F 1 "~" H 3850 13000 50  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 3850 13000 50  0001 C CNN
F 3 "" H 3850 13000 50  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    3850 13000
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x08 JM2
U 1 1 5C691CDB
P 1300 11550
F 0 "JM2" H 1350 11050 60  0000 R TNN
F 1 "8P" H 1350 12000 60  0000 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 1500 11700 60  0001 C CNN
F 3 "" H 1500 11700 60  0000 C CNN
	1    1300 11550
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US R14
U 1 1 5C691CDA
P 1750 11850
F 0 "R14" H 1800 11750 60  0000 L BNN
F 1 "1K" H 1800 11850 60  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2050 11630 60  0001 C CNN
F 3 "" H 2050 11630 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    1750 11850
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_ALT D5
U 1 1 5C691CD9
P 1750 12200
F 0 "D5" V 1750 12300 60  0000 L BNN
F 1 "~" H 1750 12200 50  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 1750 12200 50  0001 C CNN
F 3 "" H 1750 12200 50  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    1750 12200
	0    -1   -1   0   
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 U1
U 1 1 5C691CD8
P 3500 7450
F 0 "U1" H 3250 8100 60  0000 L BNN
F 1 "A4988" H 3800 6650 60  0000 L BNN
F 2 "pololu:pololu16" H 3700 5350 60  0001 C CNN
F 3 "" H 3700 5350 60  0000 C CNN
	1    3500 7450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even JK1
U 1 1 5C691CD7
P 2800 7850
F 0 "JK1" H 2800 7600 60  0000 L BNN
F 1 "JP6" H 2800 8050 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 2790 7480 60  0001 C CNN
F 3 "" H 2790 7480 60  0000 C CNN
	1    2800 7850
	-1   0    0    1   
$EndComp
$Comp
L Device:D_ALT D1
U 1 1 5C691CD6
P 4100 2700
F 0 "D1" H 4050 2550 60  0000 L BNN
F 1 "1N4004 (M4)" H 3600 2800 60  0000 L BNN
F 2 "Diode_SMD:D_SMA" H 4190 2520 60  0001 C CNN
F 3 "" H 4190 2520 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    4100 2700
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US R2
U 1 1 5C691CD5
P 2850 7150
F 0 "R2" H 2800 7250 60  0000 R TNN
F 1 "10K" H 2800 7150 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2810 7085 60  0001 C CNN
F 3 "" H 2810 7085 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    2850 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R1
U 1 1 5C691CD4
P 3050 8150
F 0 "R1" H 3000 8250 60  0000 R TNN
F 1 "100K" H 3000 8150 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3010 8085 60  0001 C CNN
F 3 "" H 3010 8085 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    3050 8150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J14
U 1 1 5C691CD3
P 4250 7550
F 0 "J14" H 4300 7185 60  0000 L BNN
F 1 "X-MOT" H 4200 7785 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4200 6900 60  0001 C CNN
F 3 "" H 4200 6900 60  0000 C CNN
	1    4250 7550
	1    0    0    1   
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 U2
U 1 1 5C691CD2
P 8850 7600
F 0 "U2" H 8550 8250 60  0000 L BNN
F 1 "A4988" H 9100 6800 60  0000 L BNN
F 2 "pololu:pololu16" H 9100 5550 60  0001 C CNN
F 3 "" H 9100 5550 60  0000 C CNN
	1    8850 7600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even JK2
U 1 1 5C691CD1
P 8150 8000
F 0 "JK2" H 8200 8200 60  0000 L BNN
F 1 "JP6" H 8150 7750 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 8140 7630 60  0001 C CNN
F 3 "" H 8140 7630 60  0000 C CNN
	1    8150 8000
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_US R4
U 1 1 5C691CD0
P 8300 7300
F 0 "R4" H 8230 7335 60  0000 R TNN
F 1 "10K" H 8260 7235 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8260 7235 60  0001 C CNN
F 3 "" H 8260 7235 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    8300 7300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R3
U 1 1 5C691CCF
P 8400 8300
F 0 "R3" H 8330 8335 60  0000 R TNN
F 1 "100K" H 8360 8235 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8360 8235 60  0001 C CNN
F 3 "" H 8360 8235 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    8400 8300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J15
U 1 1 5C691CCE
P 9600 7700
F 0 "J15" H 9550 7350 60  0000 L BNN
F 1 "Y-MOT" H 9550 7900 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 9550 7050 60  0001 C CNN
F 3 "" H 9550 7050 60  0000 C CNN
	1    9600 7700
	1    0    0    1   
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 U3
U 1 1 5C691CCD
P 14100 7200
F 0 "U3" H 13800 7850 60  0000 L BNN
F 1 "A4988" H 14350 6400 60  0000 L BNN
F 2 "pololu:pololu16" H 14400 5100 60  0001 C CNN
F 3 "" H 14400 5100 60  0000 C CNN
	1    14100 7200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even JK3
U 1 1 5C691CCC
P 13400 7600
F 0 "JK3" H 13400 7350 60  0000 L BNN
F 1 "JP6" H 13350 7800 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 13390 7230 60  0001 C CNN
F 3 "" H 13390 7230 60  0000 C CNN
	1    13400 7600
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5C691CCB
P 4650 1950
F 0 "J4" H 4650 2000 60  0000 L BNN
F 1 "12V-AUX" H 4650 1700 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4550 1600 60  0001 C CNN
F 3 "" H 4550 1600 60  0000 C CNN
	1    4650 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R6
U 1 1 5C691CCA
P 13550 6900
F 0 "R6" H 13500 6950 60  0000 R TNN
F 1 "10K" H 13510 6835 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 13510 6835 60  0001 C CNN
F 3 "" H 13510 6835 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    13550 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R5
U 1 1 5C691CC9
P 13650 7900
F 0 "R5" H 13580 7935 60  0000 R TNN
F 1 "100K" H 13610 7835 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 13610 7835 60  0001 C CNN
F 3 "" H 13610 7835 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    13650 7900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J16
U 1 1 5C691CC8
P 15050 7300
F 0 "J16" H 15000 6950 60  0000 L BNN
F 1 "Z-MOT" H 14950 7500 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 15000 6650 60  0001 C CNN
F 3 "" H 15000 6650 60  0000 C CNN
	1    15050 7300
	1    0    0    1   
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 U4
U 1 1 5C691CC7
P 19700 7450
F 0 "U4" H 19400 8100 60  0000 L BNN
F 1 "A4988" H 19950 6650 60  0000 L BNN
F 2 "pololu:pololu16" H 20000 5350 60  0001 C CNN
F 3 "" H 20000 5350 60  0000 C CNN
	1    19700 7450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even JK4
U 1 1 5C691CC6
P 19000 7850
F 0 "JK4" H 19000 7600 60  0000 L BNN
F 1 "JP6" H 18950 8050 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 18990 7480 60  0001 C CNN
F 3 "" H 18990 7480 60  0000 C CNN
	1    19000 7850
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US R8
U 1 1 5C691CC5
P 19150 7150
F 0 "R8" H 19100 7200 60  0000 R TNN
F 1 "10K" H 19110 7085 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 19110 7085 60  0001 C CNN
F 3 "" H 19110 7085 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    19150 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R7
U 1 1 5C691CC4
P 19250 8150
F 0 "R7" H 19200 8200 60  0000 R TNN
F 1 "100K" H 19210 8085 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 19210 8085 60  0001 C CNN
F 3 "" H 19210 8085 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    19250 8150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J18
U 1 1 5C691CC3
P 20450 7550
F 0 "J18" H 20400 7200 60  0000 L BNN
F 1 "E0-MOT" H 20400 7750 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 20400 6900 60  0001 C CNN
F 3 "" H 20400 6900 60  0000 C CNN
	1    20450 7550
	1    0    0    1   
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 U5
U 1 1 5C691CC2
P 19750 11500
F 0 "U5" H 19500 12150 60  0000 L BNN
F 1 "A4988" H 20000 10685 60  0000 L BNN
F 2 "pololu:pololu16" H 20050 9400 60  0001 C CNN
F 3 "" H 20050 9400 60  0000 C CNN
	1    19750 11500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even JK5
U 1 1 5C691CC1
P 19050 11900
F 0 "JK5" H 19050 11650 60  0000 L BNN
F 1 "JP6" H 19050 12100 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 19040 11530 60  0001 C CNN
F 3 "" H 19040 11530 60  0000 C CNN
	1    19050 11900
	-1   0    0    1   
$EndComp
$Comp
L Device:CP1 C2
U 1 1 5C691CC0
P 6200 3100
F 0 "C2" H 6250 3200 60  0000 L BNN
F 1 "35V/100UF" H 6250 2950 60  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 6250 2880 60  0001 C CNN
F 3 "" H 6250 2880 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    6200 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R10
U 1 1 5C691CBF
P 19200 11200
F 0 "R10" H 19150 11250 60  0000 R TNN
F 1 "10K" H 19160 11135 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 19160 11135 60  0001 C CNN
F 3 "" H 19160 11135 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    19200 11200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R9
U 1 1 5C691CBE
P 19300 12200
F 0 "R9" H 19200 12250 60  0000 R TNN
F 1 "100K" H 19260 12135 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 19260 12135 60  0001 C CNN
F 3 "" H 19260 12135 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    19300 12200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J25
U 1 1 5C691CBD
P 20500 11600
F 0 "J25" H 20450 11215 60  0000 L BNN
F 1 "E1-MOT" H 20450 11815 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 20450 10950 60  0001 C CNN
F 3 "" H 20450 10950 60  0000 C CNN
	1    20500 11600
	1    0    0    1   
$EndComp
$Comp
L Device:CP1 C6
U 1 1 5C691CBC
P 4050 6900
F 0 "C6" H 4200 6780 60  0000 L BNN
F 1 "35V/100UF" H 4100 6680 60  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 4100 6680 60  0001 C CNN
F 3 "" H 4100 6680 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    4050 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C7
U 1 1 5C691CBB
P 9400 7050
F 0 "C7" H 9550 6930 60  0000 L BNN
F 1 "35V/100UF" H 9450 6830 60  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 9450 6830 60  0001 C CNN
F 3 "" H 9450 6830 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    9400 7050
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C8
U 1 1 5C691CBA
P 14650 6650
F 0 "C8" H 14800 6530 60  0000 L BNN
F 1 "35V/100UF" H 14700 6430 60  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 14700 6430 60  0001 C CNN
F 3 "" H 14700 6430 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    14650 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C9
U 1 1 5C691CB9
P 20250 6900
F 0 "C9" H 20400 6780 60  0000 L BNN
F 1 "35V/100UF" H 20300 6680 60  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 20300 6680 60  0001 C CNN
F 3 "" H 20300 6680 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    20250 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C10
U 1 1 5C691CB8
P 20300 10950
F 0 "C10" H 20450 10830 60  0000 L BNN
F 1 "35V/100UF" H 20350 10730 60  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 20350 10730 60  0001 C CNN
F 3 "" H 20350 10730 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    20300 10950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J17
U 1 1 5C691CB7
P 15050 7950
F 0 "J17" H 15000 7615 60  0000 L BNN
F 1 "Z-MOT" H 14950 8165 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 14950 7300 60  0001 C CNN
F 3 "" H 14950 7300 60  0000 C CNN
	1    15050 7950
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J19
U 1 1 5C691CB6
P 7050 10900
F 0 "J19" V 7150 10900 60  0000 R TNN
F 1 "X-" V 7150 10700 60  0000 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7150 10700 60  0001 C CNN
F 3 "" H 7150 10700 60  0000 C CNN
	1    7050 10900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C1
U 1 1 5C691CB5
P 5750 3100
F 0 "C1" H 5900 3050 60  0000 R TNN
F 1 "0.1UF" H 5700 3250 60  0000 R TNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5700 3150 60  0001 C CNN
F 3 "" H 5700 3150 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    5750 3100
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J20
U 1 1 5C691CB4
P 7650 10900
F 0 "J20" V 7740 10900 60  0000 R TNN
F 1 "X+" V 7750 10700 60  0000 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7750 10700 60  0001 C CNN
F 3 "" H 7750 10700 60  0000 C CNN
	1    7650 10900
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J21
U 1 1 5C691CB3
P 8200 10900
F 0 "J21" V 8290 10900 60  0000 R TNN
F 1 "Y-" V 8300 10700 60  0000 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8300 10700 60  0001 C CNN
F 3 "" H 8300 10700 60  0000 C CNN
	1    8200 10900
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J22
U 1 1 5C691CB2
P 8800 10900
F 0 "J22" V 8890 10900 60  0000 R TNN
F 1 "Y+" V 8900 10700 60  0000 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8900 10700 60  0001 C CNN
F 3 "" H 8900 10700 60  0000 C CNN
	1    8800 10900
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J23
U 1 1 5C691CB1
P 9400 10900
F 0 "J23" V 9490 10900 60  0000 R TNN
F 1 "Z-" V 9500 10700 60  0000 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 9500 10700 60  0001 C CNN
F 3 "" H 9500 10700 60  0000 C CNN
	1    9400 10900
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J24
U 1 1 5C691CB0
P 10000 10900
F 0 "J24" V 10090 10900 60  0000 R TNN
F 1 "Z+" V 10100 10700 60  0000 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 10100 10700 60  0001 C CNN
F 3 "" H 10100 10700 60  0000 C CNN
	1    10000 10900
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J29
U 1 1 5C691CAF
P 2850 15350
F 0 "J29" H 2800 15550 60  0000 L BNN
F 1 "I2C" H 2800 15000 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2850 15400 60  0001 C CNN
F 3 "" H 2850 15400 60  0000 C CNN
	1    2850 15350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R24
U 1 1 5C691CAE
P 2250 15050
F 0 "R24" H 2200 15100 60  0000 R TNN
F 1 "4.7K" H 2200 15000 60  0000 R TNN
F 2 "" H 2250 15270 60  0001 C CNN
F 3 "" H 2250 15270 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    2250 15050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R25
U 1 1 5C691CAD
P 2550 15050
F 0 "R25" H 2500 15100 60  0000 R TNN
F 1 "4.7K" H 2500 15000 60  0000 R TNN
F 2 "" H 2550 15270 60  0001 C CNN
F 3 "" H 2550 15270 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    2550 15050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J27
U 1 1 5C691CAC
P 8350 12700
F 0 "J27" V 8435 12950 60  0000 R TNN
F 1 "6P" V 8435 12450 60  0000 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 8550 12150 60  0001 C CNN
F 3 "" H 8550 12150 60  0000 C CNN
	1    8350 12700
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP1 C3
U 1 1 5C691CAB
P 7350 14300
F 0 "C3" H 7500 14180 60  0000 L BNN
F 1 "35V/10UF" H 7400 14080 60  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 7400 14080 60  0001 C CNN
F 3 "" H 7400 14080 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    7350 14300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J5
U 1 1 5C691CAA
P 6200 2050
F 0 "J5" H 6200 1850 60  0000 L TNN
F 1 "3P" H 6200 2300 60  0000 L TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6200 2200 60  0001 C CNN
F 3 "" H 6200 2200 60  0000 C CNN
	1    6200 2050
	1    0    0    1   
$EndComp
$Comp
L Device:R_US R20
U 1 1 5C691CA9
P 7350 13800
F 0 "R20" H 7280 13935 60  0000 R TNN
F 1 "4.7K" H 7280 13835 60  0000 R TNN
F 2 "" H 7350 14020 60  0001 C CNN
F 3 "" H 7350 14020 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    7350 13800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C4
U 1 1 5C691CA8
P 8800 14300
F 0 "C4" H 8950 14180 60  0000 L BNN
F 1 "35V/10UF" H 8850 14080 60  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 8850 14080 60  0001 C CNN
F 3 "" H 8850 14080 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    8800 14300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R22
U 1 1 5C691CA7
P 8800 13800
F 0 "R22" H 8780 13785 60  0000 R TNN
F 1 "4.7K" H 8780 13710 60  0000 R TNN
F 2 "" H 8780 13710 60  0001 C CNN
F 3 "" H 8780 13710 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    8800 13800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C5
U 1 1 5C691CA6
P 9850 14300
F 0 "C5" H 10000 14180 60  0000 L BNN
F 1 "35V/10UF" H 9900 14080 60  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 9900 14080 60  0001 C CNN
F 3 "" H 9900 14080 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    9850 14300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R21
U 1 1 5C691CA5
P 9850 13800
F 0 "R21" H 9780 13885 60  0000 R TNN
F 1 "4.7K" H 9780 13785 60  0000 R TNN
F 2 "" H 9850 14020 60  0001 C CNN
F 3 "" H 9850 14020 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    9850 13800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 JM6
U 1 1 5C691CA4
P 8450 15800
F 0 "JM6" V 8600 15350 60  0000 L BNN
F 1 "8P" V 8600 16050 60  0000 L BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 8850 15550 60  0001 C CNN
F 3 "" H 8850 15550 60  0000 C CNN
	1    8450 15800
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x08 JM4
U 1 1 5C691CA3
P 11900 13450
F 0 "JM4" H 11980 12965 60  0000 R TNN
F 1 "8P" H 11930 13915 60  0000 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 12430 13200 60  0001 C CNN
F 3 "" H 12430 13200 60  0000 C CNN
	1    11900 13450
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x08 JM3
U 1 1 5C691CA2
P 11900 11950
F 0 "JM3" H 11980 11465 60  0000 R TNN
F 1 "8P" H 11930 12415 60  0000 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 12430 11700 60  0001 C CNN
F 3 "" H 12430 11700 60  0000 C CNN
	1    11900 11950
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x08 JM1
U 1 1 5C691CA1
P 11900 10650
F 0 "JM1" H 11950 10150 60  0000 R TNN
F 1 "8P" H 11950 11100 60  0000 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 12430 10350 60  0001 C CNN
F 3 "" H 12430 10350 60  0000 C CNN
	1    11900 10650
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x06 JM5
U 1 1 5C691CA0
P 11900 14550
F 0 "JM5" H 11950 14850 60  0000 R BNN
F 1 "6P" H 11900 14100 60  0000 R BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 12400 14000 60  0001 C CNN
F 3 "" H 12400 14000 60  0000 C CNN
	1    11900 14550
	-1   0    0    -1  
$EndComp
$Comp
L Switch:SW_Push K1
U 1 1 5C691C9F
P 8100 2100
F 0 "K1" H 8100 2200 60  0000 L BNN
F 1 "RESET" H 8000 1950 60  0000 L BNN
F 2 "Button_Switch_THT:SW_Tactile_SPST_Angled_PTS645Vx31-2LFS" H 8000 1950 60  0001 C CNN
F 3 "" H 8000 1950 60  0000 C CNN
	1    8100 2100
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J10
U 1 1 5C691C9E
P 1400 2700
F 0 "J10" H 1450 2800 60  0000 R BNN
F 1 "300V/20A" H 1650 2450 60  0000 R BNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-2-5.08_1x02_P5.08mm_Horizontal" H 1700 2390 60  0001 C CNN
F 3 "" H 1700 2390 60  0000 C CNN
F 4 "Phoenix Contact" H 1400 2700 50  0001 C CNN "DK_Mfr"
F 5 "1715721" H 1400 2700 50  0001 C CNN "DK_Mfr_PN"
	1    1400 2700
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J28
U 1 1 5C691C9D
P 5150 13200
F 0 "J28" H 5100 13000 60  0000 L TNN
F 1 "300V/20A" H 4950 13350 60  0000 L TNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-2-5.08_1x02_P5.08mm_Horizontal" H 5300 13200 60  0001 C CNN
F 3 "" H 5300 13200 60  0000 C CNN
F 4 "Phoenix Contact" H 0   100 50  0001 C CNN "DK_Mfr"
F 5 "1715721" H 0   100 50  0001 C CNN "DK_Mfr_PN"
	1    5150 13200
	1    0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x04 J26
U 1 1 5C691C9C
P 5150 11800
F 0 "J26" H 5100 12050 60  0000 L TNN
F 1 "5.08-4" H 5000 11500 60  0000 L TNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-4-5.08_1x04_P5.08mm_Horizontal" H 5380 11680 60  0001 C CNN
F 3 "" H 5380 11680 60  0000 C CNN
F 4 "Phoenix Contact" H 0   -100 50  0001 C CNN "DK_Mfr"
F 5 "1715747" H 0   -100 50  0001 C CNN "DK_Mfr_PN"
	1    5150 11800
	1    0    0    -1  
$EndComp
Text Notes 11400 15700 0    132  ~ 26
to Arduino Mega
Wire Wire Line
	5750 3250 5750 3500
Wire Wire Line
	6200 3500 6200 3250
Wire Wire Line
	5750 2950 5750 2700
Wire Wire Line
	5750 2700 5300 2700
Wire Wire Line
	5750 2700 6200 2700
Wire Wire Line
	6200 2700 6200 2950
Connection ~ 5750 2700
Text Label 5300 2700 0    50   ~ 0
+12V
Wire Wire Line
	6000 2150 5500 2150
Text Label 5500 2150 0    50   ~ 0
PS-ON
Wire Wire Line
	6000 2050 5500 2050
Text Label 5500 2050 0    50   ~ 0
+5V
Wire Wire Line
	6000 1950 5500 1950
Text Label 5500 1950 0    50   ~ 0
VCC
Wire Wire Line
	4250 2700 4700 2700
Text Label 4400 2700 0    50   ~ 0
AM-VIN
Wire Wire Line
	1600 2800 1700 2800
Wire Wire Line
	1700 2800 1700 2850
Wire Wire Line
	1600 2700 1750 2700
Wire Wire Line
	1750 2700 1750 2250
Wire Wire Line
	1750 2250 2000 2250
Wire Wire Line
	1750 2700 2350 2700
Connection ~ 1750 2700
Wire Wire Line
	2300 2250 3000 2250
Text Label 2750 2250 0    50   ~ 0
+12V2
Wire Wire Line
	3800 3150 3800 3400
Wire Wire Line
	2650 2700 3800 2700
Wire Wire Line
	3800 2850 3800 2700
Connection ~ 3800 2700
Wire Wire Line
	4400 2100 4400 2050
Wire Wire Line
	4400 2050 4450 2050
Text Label 3250 2700 0    50   ~ 0
+12V
Wire Wire Line
	8300 2100 8650 2100
Wire Wire Line
	7750 2250 7750 2100
Wire Wire Line
	7750 2100 7900 2100
Text Label 8450 2100 0    50   ~ 0
RESET
Wire Wire Line
	10450 1750 10500 1750
Wire Wire Line
	10450 2300 10500 2300
Wire Wire Line
	10450 2800 10500 2800
Wire Wire Line
	10450 3500 10450 3450
Wire Wire Line
	10450 3450 10500 3450
Wire Wire Line
	10500 2700 10400 2700
Wire Wire Line
	10500 3350 10400 3350
Wire Wire Line
	10500 2200 10400 2200
Wire Wire Line
	10500 1650 10400 1650
Text Label 10100 1650 0    50   ~ 0
+5V
Wire Wire Line
	10500 3250 10100 3250
Text Label 10100 3250 0    50   ~ 0
D4
Wire Wire Line
	10500 2600 10100 2600
Text Label 10100 2600 0    50   ~ 0
D5
Wire Wire Line
	10500 2100 10100 2100
Text Label 10100 2100 0    50   ~ 0
D6
Wire Wire Line
	10500 1550 10100 1550
Text Label 10100 1550 0    50   ~ 0
D11
Wire Wire Line
	3800 2700 3900 2700
Wire Wire Line
	4450 1950 3900 1950
Wire Wire Line
	3900 1950 3900 2700
Connection ~ 3900 2700
Wire Wire Line
	3900 2700 3950 2700
Wire Wire Line
	12550 2100 12450 2100
Wire Wire Line
	12550 2200 12500 2200
Wire Wire Line
	12550 2300 12250 2300
Wire Wire Line
	12550 2400 12250 2400
Text Label 12250 2100 0    50   ~ 0
VCC
Text Label 12250 2300 0    50   ~ 0
A3
Text Label 12250 2400 0    50   ~ 0
A4
Wire Wire Line
	12550 3150 12450 3150
Wire Wire Line
	12550 3250 12500 3250
Wire Wire Line
	12550 3350 12250 3350
Wire Wire Line
	12550 3450 12250 3450
Text Label 12250 3350 0    50   ~ 0
D2
Text Label 12250 3450 0    50   ~ 0
D1
Wire Wire Line
	14450 2950 14750 2950
Wire Wire Line
	14450 3050 14750 3050
Wire Wire Line
	14450 3150 14750 3150
Wire Wire Line
	14450 3250 14750 3250
Text Label 14650 3250 0    50   ~ 0
A11
Text Label 14650 3150 0    50   ~ 0
D42
Text Label 14650 3050 0    50   ~ 0
D40
Text Label 14700 2950 0    50   ~ 0
A9
Wire Wire Line
	13650 2850 13950 2850
Wire Wire Line
	13650 2950 13950 2950
Wire Wire Line
	13650 3050 13950 3050
Wire Wire Line
	13650 3150 13950 3150
Wire Wire Line
	13650 3250 13950 3250
Text Label 13650 2850 0    50   ~ 0
VCC
Text Label 13650 2950 0    50   ~ 0
A5
Text Label 13650 3050 0    50   ~ 0
A10
Text Label 13650 3150 0    50   ~ 0
D44
Text Label 13650 3250 0    50   ~ 0
A12
Wire Wire Line
	15600 2650 15900 2650
Wire Wire Line
	15600 2750 15900 2750
Wire Wire Line
	15600 2850 15900 2850
Wire Wire Line
	16400 2850 16700 2850
Wire Wire Line
	16400 2750 16700 2750
Wire Wire Line
	16400 2650 16700 2650
Text Label 15600 2650 0    50   ~ 0
VCC
Text Label 15600 2750 0    50   ~ 0
MISO
Text Label 15600 2850 0    50   ~ 0
SCK
Wire Wire Line
	15850 3000 15850 2950
Wire Wire Line
	15850 2950 15900 2950
NoConn ~ 16400 2950
Text Label 16600 2650 0    50   ~ 0
D49
Text Label 16550 2750 0    50   ~ 0
MOSI
Text Label 16600 2850 0    50   ~ 0
D53
Wire Wire Line
	18100 1900 18400 1900
Wire Wire Line
	18100 2000 18400 2000
Wire Wire Line
	18100 2100 18400 2100
Wire Wire Line
	18100 2200 18400 2200
Wire Wire Line
	18100 2300 18400 2300
Wire Wire Line
	18100 2400 18400 2400
Wire Wire Line
	18100 2500 18400 2500
Wire Wire Line
	18100 2600 18400 2600
Wire Wire Line
	18100 2700 18400 2700
Wire Wire Line
	18100 2800 18400 2800
Wire Wire Line
	18100 2900 18400 2900
Wire Wire Line
	18100 3000 18400 3000
Wire Wire Line
	18100 3100 18400 3100
Wire Wire Line
	18100 3200 18400 3200
Wire Wire Line
	18100 3300 18400 3300
Wire Wire Line
	18100 3400 18400 3400
Wire Wire Line
	18100 3600 18400 3600
Text Label 18100 1900 0    50   ~ 0
D16
Text Label 18100 2000 0    50   ~ 0
D17
Text Label 18100 2100 0    50   ~ 0
D23
Text Label 18100 2200 0    50   ~ 0
D25
Text Label 18100 2300 0    50   ~ 0
D27
Text Label 18100 2400 0    50   ~ 0
D29
Text Label 18100 2500 0    50   ~ 0
D31
Text Label 18100 2600 0    50   ~ 0
D33
Text Label 18100 2700 0    50   ~ 0
D35
Text Label 18100 2800 0    50   ~ 0
D37
Text Label 18100 2900 0    50   ~ 0
D39
Text Label 18100 3000 0    50   ~ 0
D41
Text Label 18100 3100 0    50   ~ 0
D43
Text Label 18100 3200 0    50   ~ 0
D45
Text Label 18100 3300 0    50   ~ 0
D47
Text Label 18100 3400 0    50   ~ 0
D32
Text Label 18100 3600 0    50   ~ 0
VCC
Wire Wire Line
	18000 3500 18400 3500
Wire Wire Line
	21850 3550 21550 3550
Wire Wire Line
	21850 3450 21550 3450
Wire Wire Line
	21850 3350 21550 3350
Wire Wire Line
	21850 3250 21550 3250
Wire Wire Line
	21850 3150 21550 3150
Wire Wire Line
	21850 3050 21550 3050
Wire Wire Line
	21850 2950 21550 2950
Wire Wire Line
	21850 2850 21550 2850
Wire Wire Line
	21850 2750 21550 2750
Wire Wire Line
	21850 2650 21550 2650
Wire Wire Line
	21850 2550 21550 2550
Wire Wire Line
	21850 2450 21550 2450
Wire Wire Line
	21850 2350 21550 2350
Wire Wire Line
	21850 2250 21550 2250
Wire Wire Line
	21850 2150 21550 2150
Wire Wire Line
	21850 2050 21550 2050
Wire Wire Line
	21850 1950 21600 1950
Wire Wire Line
	21050 3550 20750 3550
Wire Wire Line
	21050 3450 20750 3450
Wire Wire Line
	21050 3350 20750 3350
Wire Wire Line
	21050 3250 20750 3250
Wire Wire Line
	21050 3150 20750 3150
Wire Wire Line
	21050 3050 20750 3050
Wire Wire Line
	21050 2950 20750 2950
Wire Wire Line
	21050 2850 20750 2850
Wire Wire Line
	21050 2750 20750 2750
Wire Wire Line
	21050 2650 20750 2650
Wire Wire Line
	21050 2550 20750 2550
Wire Wire Line
	21050 2450 20750 2450
Wire Wire Line
	21050 2350 20750 2350
Wire Wire Line
	21050 2250 20750 2250
Wire Wire Line
	21050 2150 20750 2150
Wire Wire Line
	21050 3650 21050 3750
Wire Wire Line
	21050 3750 21600 3750
Wire Wire Line
	21600 3750 21600 3650
Wire Wire Line
	21600 3650 21550 3650
Text Label 21850 3550 2    50   ~ 0
D53
Text Label 21850 3450 2    50   ~ 0
MOSI
Text Label 21850 3350 2    50   ~ 0
D49
Text Label 21850 3250 2    50   ~ 0
D47
Text Label 21850 3150 2    50   ~ 0
D45
Text Label 21850 3050 2    50   ~ 0
D43
Text Label 21850 2950 2    50   ~ 0
D41
Text Label 21850 2850 2    50   ~ 0
D39
Text Label 21850 2750 2    50   ~ 0
D37
Text Label 21850 2650 2    50   ~ 0
D35
Text Label 21850 2550 2    50   ~ 0
D33
Text Label 21850 2450 2    50   ~ 0
D31
Text Label 21850 2350 2    50   ~ 0
D29
Text Label 21850 2250 2    50   ~ 0
D27
Text Label 21850 2150 2    50   ~ 0
D25
Text Label 21850 2050 2    50   ~ 0
D23
Text Label 21850 1950 2    50   ~ 0
VCC
Wire Wire Line
	21050 1950 21050 1850
Wire Wire Line
	21050 1850 21600 1850
Wire Wire Line
	21600 1850 21600 1950
Connection ~ 21600 1950
Wire Wire Line
	21600 1950 21550 1950
Wire Wire Line
	21600 3750 21600 3800
Connection ~ 21600 3750
Wire Wire Line
	21050 2050 20750 2050
Text Label 20750 2050 0    50   ~ 0
D22
Text Label 20750 2150 0    50   ~ 0
E0-EN
Text Label 20750 2250 0    50   ~ 0
E0-STEP
Text Label 20750 2350 0    50   ~ 0
E0-DIR
Text Label 20750 2450 0    50   ~ 0
E1-EN
Text Label 20750 2550 0    50   ~ 0
D32
Text Label 20750 2650 0    50   ~ 0
E1-DIR
Text Label 20750 2750 0    50   ~ 0
E1-STEP
Text Label 20750 2850 0    50   ~ 0
X-EN
Text Label 20750 2950 0    50   ~ 0
D40
Text Label 20750 3050 0    50   ~ 0
D42
Text Label 20750 3150 0    50   ~ 0
D44
Text Label 20750 3250 0    50   ~ 0
Z-STEP
Text Label 20750 3350 0    50   ~ 0
Z-DIR
Text Label 20750 3450 0    50   ~ 0
MISO
Text Label 20750 3550 0    50   ~ 0
SCK
Wire Wire Line
	4000 7350 4050 7350
Wire Wire Line
	4050 7450 4000 7450
Wire Wire Line
	4000 7550 4050 7550
Wire Wire Line
	4050 7650 4000 7650
Wire Wire Line
	3500 8250 3500 8300
Wire Wire Line
	3700 8300 3700 8250
Wire Wire Line
	3500 6450 3500 6750
Wire Wire Line
	3700 6450 3700 6750
Text Label 3500 6600 1    50   ~ 0
VCC
Text Label 3700 6650 1    50   ~ 0
+12V
Wire Wire Line
	3100 7050 3050 7050
Wire Wire Line
	3050 7050 3050 7150
Wire Wire Line
	3050 7150 3100 7150
Wire Wire Line
	3050 8350 3050 8300
Wire Wire Line
	2500 7950 2450 7950
Wire Wire Line
	2450 7950 2450 7850
Wire Wire Line
	2450 7750 2500 7750
Wire Wire Line
	2500 7850 2450 7850
Connection ~ 2450 7850
Wire Wire Line
	2450 7850 2450 7750
Wire Wire Line
	2450 7750 2100 7750
Connection ~ 2450 7750
Text Label 2100 7750 0    50   ~ 0
VCC
Wire Wire Line
	3100 7350 2850 7350
Wire Wire Line
	2850 7350 2850 7300
Text Label 2850 6600 1    50   ~ 0
VCC
Wire Wire Line
	4050 6450 4050 6750
Text Label 4050 6650 1    50   ~ 0
+12V
Wire Wire Line
	4050 7100 4050 7050
Wire Wire Line
	8450 7200 8400 7200
Wire Wire Line
	8400 7200 8400 7300
Wire Wire Line
	8400 7300 8450 7300
Wire Wire Line
	8850 8400 8850 8450
Wire Wire Line
	9050 8400 9050 8450
Wire Wire Line
	9350 7500 9400 7500
Wire Wire Line
	9400 7600 9350 7600
Wire Wire Line
	9350 7700 9400 7700
Wire Wire Line
	9400 7800 9350 7800
Wire Wire Line
	8400 8450 8400 8500
Wire Wire Line
	8450 7900 8400 7900
Wire Wire Line
	8400 7900 8400 8150
Connection ~ 8400 7900
Wire Wire Line
	8400 7900 8350 7900
Wire Wire Line
	8350 8100 8450 8100
Wire Wire Line
	8450 8000 8350 8000
Wire Wire Line
	7850 7900 7800 7900
Text Label 7550 7900 0    50   ~ 0
VCC
Wire Wire Line
	7850 8100 7800 8100
Wire Wire Line
	7800 8100 7800 8000
Connection ~ 7800 7900
Wire Wire Line
	7800 7900 7550 7900
Wire Wire Line
	7850 8000 7800 8000
Connection ~ 7800 8000
Wire Wire Line
	7800 8000 7800 7900
Wire Wire Line
	8450 7700 7550 7700
Wire Wire Line
	8450 7600 7550 7600
Wire Wire Line
	8450 7500 8300 7500
Wire Wire Line
	8300 7450 8300 7500
Connection ~ 8300 7500
Wire Wire Line
	8300 7500 7550 7500
Wire Wire Line
	8850 6600 8850 6900
Wire Wire Line
	9050 6600 9050 6900
Text Label 8850 6750 1    50   ~ 0
VCC
Text Label 9050 6800 1    50   ~ 0
+12V
Text Label 8300 6750 1    50   ~ 0
VCC
Wire Wire Line
	8300 6600 8300 7150
Text Label 7550 7500 0    50   ~ 0
Y-EN
Text Label 7550 7600 0    50   ~ 0
Y-STEP
Text Label 7550 7700 0    50   ~ 0
Y-DIR
Wire Wire Line
	9400 6600 9400 6900
Text Label 9400 6800 1    50   ~ 0
+12V
Wire Wire Line
	9400 7200 9400 7250
Wire Wire Line
	3000 7750 3050 7750
Wire Wire Line
	3100 7850 3000 7850
Wire Wire Line
	3000 7950 3100 7950
Wire Wire Line
	3050 8000 3050 7750
Connection ~ 3050 7750
Wire Wire Line
	3050 7750 3100 7750
Connection ~ 2850 7350
Wire Wire Line
	3100 7550 2100 7550
Wire Wire Line
	2100 7450 3100 7450
Wire Wire Line
	2100 7350 2850 7350
Text Label 2100 7350 0    50   ~ 0
X-EN
Text Label 2100 7450 0    50   ~ 0
X-STEP
Text Label 2100 7550 0    50   ~ 0
X-DIR
Wire Wire Line
	2850 6450 2850 7000
Wire Wire Line
	14850 7400 14800 7400
Wire Wire Line
	14800 7400 14800 8050
Wire Wire Line
	14800 8050 14850 8050
Wire Wire Line
	14850 7950 14750 7950
Wire Wire Line
	14750 7950 14750 7300
Wire Wire Line
	14750 7300 14850 7300
Wire Wire Line
	14850 7850 14700 7850
Wire Wire Line
	14700 7850 14700 7200
Wire Wire Line
	14700 7200 14850 7200
Wire Wire Line
	14850 7750 14650 7750
Wire Wire Line
	14650 7750 14650 7100
Wire Wire Line
	14650 7100 14850 7100
Wire Wire Line
	14600 7100 14650 7100
Connection ~ 14650 7100
Wire Wire Line
	14600 7200 14700 7200
Connection ~ 14700 7200
Wire Wire Line
	14600 7300 14750 7300
Connection ~ 14750 7300
Wire Wire Line
	14600 7400 14800 7400
Connection ~ 14800 7400
Wire Wire Line
	13600 7500 13650 7500
Wire Wire Line
	13700 7600 13600 7600
Wire Wire Line
	13600 7700 13700 7700
Wire Wire Line
	13650 7750 13650 7500
Connection ~ 13650 7500
Wire Wire Line
	13650 7500 13700 7500
Wire Wire Line
	13650 8050 13650 8100
Wire Wire Line
	14100 8000 14100 8050
Wire Wire Line
	14300 8000 14300 8050
Wire Wire Line
	13100 7700 13050 7700
Wire Wire Line
	13050 7700 13050 7600
Wire Wire Line
	13050 7500 13100 7500
Wire Wire Line
	13100 7600 13050 7600
Connection ~ 13050 7600
Wire Wire Line
	13050 7600 13050 7500
Text Label 12800 7500 0    50   ~ 0
VCC
Wire Wire Line
	13050 7500 12800 7500
Wire Wire Line
	13700 7300 12800 7300
Wire Wire Line
	13700 7200 12800 7200
Wire Wire Line
	13550 7100 12800 7100
Text Label 12800 7100 0    50   ~ 0
Z-EN
Text Label 12800 7200 0    50   ~ 0
Z-STEP
Text Label 12800 7300 0    50   ~ 0
Z-DIR
Wire Wire Line
	13700 6800 13650 6800
Wire Wire Line
	13650 6800 13650 6900
Wire Wire Line
	13650 6900 13700 6900
Wire Wire Line
	13550 7050 13550 7100
Wire Wire Line
	13550 7100 13700 7100
Connection ~ 13550 7100
Wire Wire Line
	14100 6200 14100 6500
Wire Wire Line
	14300 6200 14300 6500
Text Label 14100 6350 1    50   ~ 0
VCC
Text Label 14300 6400 1    50   ~ 0
+12V
Text Label 13550 6350 1    50   ~ 0
VCC
Wire Wire Line
	13550 6200 13550 6750
Wire Wire Line
	14650 6200 14650 6500
Text Label 14650 6400 1    50   ~ 0
+12V
Wire Wire Line
	14650 6800 14650 6850
Wire Wire Line
	19300 7050 19250 7050
Wire Wire Line
	19250 7050 19250 7150
Wire Wire Line
	19250 7150 19300 7150
Wire Wire Line
	19200 7750 19250 7750
Wire Wire Line
	19300 7850 19200 7850
Wire Wire Line
	19200 7950 19300 7950
Wire Wire Line
	19250 8000 19250 7750
Connection ~ 19250 7750
Wire Wire Line
	19250 7750 19300 7750
Wire Wire Line
	19250 8300 19250 8350
Wire Wire Line
	19700 8300 19700 8250
Wire Wire Line
	19900 8250 19900 8300
Wire Wire Line
	20200 7350 20250 7350
Wire Wire Line
	20250 7450 20200 7450
Wire Wire Line
	20200 7550 20250 7550
Wire Wire Line
	20250 7650 20200 7650
Wire Wire Line
	19700 6450 19700 6750
Wire Wire Line
	19900 6450 19900 6750
Text Label 19700 6600 1    50   ~ 0
VCC
Text Label 19900 6650 1    50   ~ 0
+12V
Text Label 19150 6600 1    50   ~ 0
VCC
Wire Wire Line
	19150 6450 19150 7000
Wire Wire Line
	20250 6450 20250 6750
Text Label 20250 6650 1    50   ~ 0
+12V
Wire Wire Line
	20250 7050 20250 7100
Wire Wire Line
	19300 7550 18400 7550
Wire Wire Line
	19300 7450 18400 7450
Wire Wire Line
	19150 7350 18400 7350
Text Label 18400 7350 0    50   ~ 0
E0-EN
Text Label 18400 7450 0    50   ~ 0
E0-STEP
Text Label 18400 7550 0    50   ~ 0
E0-DIR
Wire Wire Line
	19150 7300 19150 7350
Wire Wire Line
	19150 7350 19300 7350
Connection ~ 19150 7350
Wire Wire Line
	18700 7950 18650 7950
Wire Wire Line
	18650 7950 18650 7850
Wire Wire Line
	18650 7750 18700 7750
Wire Wire Line
	18700 7850 18650 7850
Connection ~ 18650 7850
Wire Wire Line
	18650 7850 18650 7750
Text Label 18400 7750 0    50   ~ 0
VCC
Wire Wire Line
	18650 7750 18400 7750
Wire Wire Line
	19750 10500 19750 10800
Wire Wire Line
	19950 10500 19950 10800
Text Label 19750 10650 1    50   ~ 0
VCC
Text Label 19950 10700 1    50   ~ 0
+12V
Text Label 19200 10650 1    50   ~ 0
VCC
Wire Wire Line
	19200 10500 19200 11050
Wire Wire Line
	20300 10500 20300 10800
Text Label 20300 10700 1    50   ~ 0
+12V
Wire Wire Line
	19350 11600 18450 11600
Wire Wire Line
	19350 11500 18450 11500
Wire Wire Line
	19200 11400 18450 11400
Text Label 18450 11400 0    50   ~ 0
E1-EN
Text Label 18450 11500 0    50   ~ 0
E1-STEP
Text Label 18450 11600 0    50   ~ 0
E1-DIR
Wire Wire Line
	18750 12000 18700 12000
Wire Wire Line
	18700 12000 18700 11900
Wire Wire Line
	18700 11800 18750 11800
Wire Wire Line
	18750 11900 18700 11900
Connection ~ 18700 11900
Wire Wire Line
	18700 11900 18700 11800
Text Label 18450 11800 0    50   ~ 0
VCC
Wire Wire Line
	18700 11800 18450 11800
Wire Wire Line
	19200 11350 19200 11400
Wire Wire Line
	19350 11400 19200 11400
Connection ~ 19200 11400
Wire Wire Line
	19250 11800 19300 11800
Wire Wire Line
	19350 11900 19250 11900
Wire Wire Line
	19250 12000 19350 12000
Wire Wire Line
	19300 12050 19300 11800
Connection ~ 19300 11800
Wire Wire Line
	19300 11800 19350 11800
Wire Wire Line
	19300 12400 19300 12350
Wire Wire Line
	19750 12300 19750 12350
Wire Wire Line
	19950 12350 19950 12300
Wire Wire Line
	20250 11700 20300 11700
Wire Wire Line
	20300 11600 20250 11600
Wire Wire Line
	20250 11500 20300 11500
Wire Wire Line
	20300 11400 20250 11400
Wire Wire Line
	20300 11150 20300 11100
Wire Wire Line
	12100 10250 12400 10250
Wire Wire Line
	12100 10350 12400 10350
Wire Wire Line
	12100 10450 12400 10450
Wire Wire Line
	12100 10550 12400 10550
Wire Wire Line
	12100 10650 12400 10650
Wire Wire Line
	12100 10750 12400 10750
Wire Wire Line
	12100 10850 12400 10850
Wire Wire Line
	12100 10950 12400 10950
Text Label 12150 10250 0    50   ~ 0
Y-DIR
Text Label 12150 10350 0    50   ~ 0
Y-STEP
Text Label 12150 10450 0    50   ~ 0
A5
Text Label 12150 10550 0    50   ~ 0
D1
Text Label 12150 10650 0    50   ~ 0
D2
Text Label 12150 10750 0    50   ~ 0
Y-EN
Text Label 12150 10850 0    50   ~ 0
X-DIR
Text Label 12150 10950 0    50   ~ 0
X-STEP
Wire Wire Line
	12100 11550 12400 11550
Text Label 12150 11550 0    50   ~ 0
A4
Wire Wire Line
	12100 11650 12400 11650
Text Label 12150 11650 0    50   ~ 0
A3
Wire Wire Line
	12100 11750 12400 11750
Text Label 12150 11750 0    50   ~ 0
X-MAX
Wire Wire Line
	12100 11850 12400 11850
Text Label 12150 11850 0    50   ~ 0
X-MIN
Wire Wire Line
	12100 11950 12400 11950
Text Label 12150 11950 0    50   ~ 0
D4
Wire Wire Line
	12100 12050 12400 12050
Text Label 12150 12050 0    50   ~ 0
D5
Wire Wire Line
	12100 12150 12400 12150
Text Label 12150 12150 0    50   ~ 0
D6
NoConn ~ 12100 12250
Wire Wire Line
	12100 13050 12400 13050
Text Label 12150 13050 0    50   ~ 0
SCL
Wire Wire Line
	12100 13150 12400 13150
Text Label 12150 13150 0    50   ~ 0
SDA
Wire Wire Line
	12100 13250 12400 13250
Text Label 12150 13250 0    50   ~ 0
Z-MAX
Wire Wire Line
	12100 13350 12400 13350
Text Label 12150 13350 0    50   ~ 0
Z-MIN
Wire Wire Line
	12100 13450 12400 13450
Text Label 12150 13450 0    50   ~ 0
D17
Wire Wire Line
	12100 13550 12400 13550
Text Label 12150 13550 0    50   ~ 0
D16
Wire Wire Line
	12100 13650 12400 13650
Text Label 12150 13650 0    50   ~ 0
Y-MAX
Wire Wire Line
	12100 13750 12400 13750
Text Label 12150 13750 0    50   ~ 0
Y-MIN
NoConn ~ 12100 14450
Wire Wire Line
	12100 14350 12400 14350
Text Label 12150 14350 0    50   ~ 0
RESET
Wire Wire Line
	12100 14550 12400 14550
Text Label 12150 14550 0    50   ~ 0
VCC
Wire Wire Line
	12100 14750 12150 14750
Wire Wire Line
	12100 14850 12400 14850
Text Label 12150 14850 0    50   ~ 0
AM-VIN
Wire Wire Line
	12100 14650 12150 14650
Wire Wire Line
	12150 14650 12150 14750
Connection ~ 12150 14750
Wire Wire Line
	12150 14750 12500 14750
Wire Wire Line
	10100 11100 10100 11150
Wire Wire Line
	10100 11150 9500 11150
Wire Wire Line
	9500 11150 9500 11100
Wire Wire Line
	9500 11150 8900 11150
Wire Wire Line
	8900 11150 8900 11100
Connection ~ 9500 11150
Wire Wire Line
	8900 11150 8300 11150
Wire Wire Line
	8300 11150 8300 11100
Connection ~ 8900 11150
Wire Wire Line
	8300 11150 7750 11150
Wire Wire Line
	7750 11150 7750 11100
Connection ~ 8300 11150
Wire Wire Line
	7750 11150 7150 11150
Wire Wire Line
	7150 11150 7150 11100
Connection ~ 7750 11150
Wire Wire Line
	6650 11150 7150 11150
Connection ~ 7150 11150
Text Label 6650 11150 0    50   ~ 0
VCC
Wire Wire Line
	7050 11100 7050 11200
Wire Wire Line
	7050 11200 7650 11200
Wire Wire Line
	7650 11100 7650 11200
Wire Wire Line
	7650 11200 8200 11200
Wire Wire Line
	8200 11200 8200 11100
Connection ~ 7650 11200
Wire Wire Line
	8200 11200 8800 11200
Wire Wire Line
	8800 11200 8800 11100
Connection ~ 8200 11200
Wire Wire Line
	8800 11200 9400 11200
Wire Wire Line
	9400 11200 9400 11100
Connection ~ 8800 11200
Wire Wire Line
	9400 11200 10000 11200
Wire Wire Line
	10000 11200 10000 11100
Connection ~ 9400 11200
Wire Wire Line
	10000 11200 10400 11200
Connection ~ 10000 11200
Text Label 6950 11550 1    50   ~ 0
X-MIN
Text Label 8100 11550 1    50   ~ 0
Y-MIN
Text Label 9300 11550 1    50   ~ 0
Z-MIN
Text Label 7550 11550 1    50   ~ 0
X-MAX
Text Label 8700 11550 1    50   ~ 0
Y-MAX
Text Label 9900 11550 1    50   ~ 0
Z-MAX
Wire Wire Line
	6950 11100 6950 11550
Wire Wire Line
	7550 11100 7550 11550
Wire Wire Line
	8100 11100 8100 11550
Wire Wire Line
	8700 11100 8700 11550
Wire Wire Line
	9300 11100 9300 11550
Wire Wire Line
	9900 11100 9900 11550
Wire Wire Line
	8150 12950 8150 12900
Wire Wire Line
	8150 12950 8350 12950
Wire Wire Line
	8350 12950 8350 12900
Wire Wire Line
	8350 12950 8550 12950
Wire Wire Line
	8550 12950 8550 12900
Connection ~ 8350 12950
Wire Wire Line
	9850 14450 9850 14500
Wire Wire Line
	8800 14450 8800 14500
Wire Wire Line
	7350 14450 7350 14500
Wire Wire Line
	8750 15600 8750 15300
Text Label 8750 15550 1    50   ~ 0
Z-EN
Wire Wire Line
	8650 15600 8650 15300
Text Label 8650 15550 1    50   ~ 0
A9
Wire Wire Line
	8550 15600 8550 15300
Text Label 8550 15550 1    50   ~ 0
A10
Wire Wire Line
	8450 15600 8450 15300
Text Label 8450 15550 1    50   ~ 0
A11
Wire Wire Line
	8350 15600 8350 15300
Text Label 8350 15550 1    50   ~ 0
A12
Wire Wire Line
	7350 13650 7350 13350
Text Label 7350 13500 1    50   ~ 0
VCC
Wire Wire Line
	7350 13950 7350 14050
Wire Wire Line
	8800 13650 8800 13350
Text Label 8800 13500 1    50   ~ 0
VCC
Wire Wire Line
	9850 13650 9850 13350
Text Label 9850 13500 1    50   ~ 0
VCC
Wire Wire Line
	8250 12900 8250 14050
Wire Wire Line
	8250 14050 7350 14050
Connection ~ 7350 14050
Wire Wire Line
	7350 14050 7350 14150
Wire Wire Line
	8250 14050 8250 15600
Connection ~ 8250 14050
Text Label 8250 14700 1    50   ~ 0
THERM0
Wire Wire Line
	8800 13950 8800 14050
Wire Wire Line
	8450 12900 8450 14050
Wire Wire Line
	8450 14050 8800 14050
Connection ~ 8800 14050
Wire Wire Line
	8800 14050 8800 14150
Wire Wire Line
	8450 14050 8450 15100
Wire Wire Line
	8450 15100 8150 15100
Wire Wire Line
	8150 15100 8150 15600
Connection ~ 8450 14050
Text Label 8450 14700 1    50   ~ 0
THERM1
Wire Wire Line
	9850 14150 9850 14050
Wire Wire Line
	8650 12900 8650 12950
Wire Wire Line
	8650 12950 9400 12950
Wire Wire Line
	9400 12950 9400 14050
Wire Wire Line
	9400 14050 9850 14050
Connection ~ 9850 14050
Wire Wire Line
	9850 14050 9850 13950
Wire Wire Line
	9400 14050 9400 15150
Wire Wire Line
	9400 15150 8050 15150
Wire Wire Line
	8050 15150 8050 15600
Connection ~ 9400 14050
Text Label 9400 14700 1    50   ~ 0
THERM2
Wire Wire Line
	2650 15250 2650 14850
Wire Wire Line
	2650 14850 2550 14850
Wire Wire Line
	2550 14850 2550 14900
Wire Wire Line
	2550 14850 2250 14850
Wire Wire Line
	2250 14850 2250 14900
Connection ~ 2550 14850
Wire Wire Line
	2250 14850 2000 14850
Connection ~ 2250 14850
Text Label 2000 14850 0    50   ~ 0
VCC
Wire Wire Line
	1900 15350 2650 15350
Wire Wire Line
	2000 15450 2250 15450
Wire Wire Line
	2000 15550 2550 15550
Text Label 2000 15450 0    50   ~ 0
SDA
Text Label 2000 15550 0    50   ~ 0
SCL
Wire Wire Line
	2250 15200 2250 15450
Connection ~ 2250 15450
Wire Wire Line
	2250 15450 2650 15450
Wire Wire Line
	2550 15200 2550 15550
Connection ~ 2550 15550
Wire Wire Line
	2550 15550 2650 15550
NoConn ~ 1500 11850
NoConn ~ 1500 11750
Wire Wire Line
	1500 11650 1750 11650
Wire Wire Line
	1750 11650 1750 11700
Wire Wire Line
	1750 12000 1750 12050
Wire Wire Line
	1750 12350 1750 12400
Wire Wire Line
	1500 11550 1750 11550
Wire Wire Line
	1500 11450 1750 11450
Text Label 1600 11450 0    50   ~ 0
D11
Text Label 1500 11550 0    50   ~ 0
PS-ON
Wire Wire Line
	1500 11350 2500 11350
Wire Wire Line
	2550 11100 2500 11100
Wire Wire Line
	2500 11100 2500 11350
Connection ~ 2500 11350
Wire Wire Line
	2500 11350 2550 11350
Wire Wire Line
	2850 11100 3200 11100
Wire Wire Line
	2850 11350 3500 11350
Wire Wire Line
	3500 11350 3500 11300
Wire Wire Line
	3500 11350 3500 11400
Connection ~ 3500 11350
Text Label 1600 11350 0    50   ~ 0
D10
Wire Wire Line
	1500 11250 2350 11250
Wire Wire Line
	2350 11250 2350 12300
Wire Wire Line
	2350 12300 2500 12300
Wire Wire Line
	2350 12300 2350 12550
Wire Wire Line
	2350 12550 2500 12550
Connection ~ 2350 12300
Wire Wire Line
	2800 12300 3200 12300
Wire Wire Line
	2800 12550 3500 12550
Wire Wire Line
	3500 12550 3500 12500
Wire Wire Line
	3500 12550 3500 12600
Connection ~ 3500 12550
Text Label 1650 11250 0    50   ~ 0
D9
$Comp
L Device:LED_ALT D4
U 1 1 5C691CDE
P 3850 11450
F 0 "D4" V 3850 11350 60  0000 R TNN
F 1 "~" H 3850 11450 50  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 3850 11450 50  0001 C CNN
F 3 "" H 3850 11450 50  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    3850 11450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 11900 3850 11900
Wire Wire Line
	3500 11900 3500 12100
$Comp
L Device:R_US R12
U 1 1 5C691CE8
P 4200 11100
F 0 "R12" H 4450 11200 60  0000 R TNN
F 1 "1.8K" H 4450 11100 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4160 11035 60  0001 C CNN
F 3 "" H 4160 11035 60  0000 C CNN
F 4 "" H 0   0   50  0001 C CNN "Package"
	1    4200 11100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 11700 4200 11700
Wire Wire Line
	3650 11700 3650 10850
Wire Wire Line
	3650 10850 3500 10850
Wire Wire Line
	3500 10850 3500 10900
Wire Wire Line
	3850 11900 3850 11600
Connection ~ 3850 11900
Wire Wire Line
	3850 11900 3500 11900
Wire Wire Line
	3850 11300 3850 11250
Wire Wire Line
	4950 12000 4900 12000
Wire Wire Line
	4900 12000 4900 11800
Wire Wire Line
	4900 11800 4950 11800
Wire Wire Line
	4200 11250 4200 11300
Wire Wire Line
	4200 11600 4200 11700
Connection ~ 4200 11700
Wire Wire Line
	4200 11700 3650 11700
Wire Wire Line
	3850 10950 3850 10850
Wire Wire Line
	3850 10850 4200 10850
Wire Wire Line
	4200 10850 4200 10950
Wire Wire Line
	4200 10850 4900 10850
Wire Wire Line
	4900 10850 4900 11800
Connection ~ 4200 10850
Connection ~ 4900 11800
Text Label 4700 10850 0    50   ~ 0
+12V
Wire Wire Line
	1500 11150 2250 11150
Wire Wire Line
	2250 11150 2250 13450
Wire Wire Line
	2250 13750 2550 13750
Wire Wire Line
	2550 13450 2250 13450
Connection ~ 2250 13450
Wire Wire Line
	2250 13450 2250 13750
Wire Wire Line
	2850 13450 3200 13450
Text Label 1650 11150 0    50   ~ 0
D8
Wire Wire Line
	2850 13750 3500 13750
Wire Wire Line
	3500 13750 3500 13650
Wire Wire Line
	3500 13750 3500 13800
Connection ~ 3500 13750
Wire Wire Line
	3500 13250 3500 13200
Wire Wire Line
	3500 13200 3850 13200
Wire Wire Line
	3850 13150 3850 13200
Connection ~ 3850 13200
Wire Wire Line
	3850 13200 4950 13200
Wire Wire Line
	3850 12850 3850 12800
Wire Wire Line
	3850 12800 4050 12800
Wire Wire Line
	4350 12800 4850 12800
Text Label 4600 12800 0    50   ~ 0
+12V2
Wire Wire Line
	4850 12800 4850 13100
Wire Wire Line
	4850 13100 4950 13100
$Comp
L power:GND #PWR0101
U 1 1 6078129C
P 1700 2850
F 0 "#PWR0101" H 1700 2600 50  0001 C CNN
F 1 "GND" H 1705 2677 50  0000 C CNN
F 2 "" H 1700 2850 50  0001 C CNN
F 3 "" H 1700 2850 50  0001 C CNN
	1    1700 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6078154E
P 3800 3400
F 0 "#PWR0102" H 3800 3150 50  0001 C CNN
F 1 "GND" H 3805 3227 50  0000 C CNN
F 2 "" H 3800 3400 50  0001 C CNN
F 3 "" H 3800 3400 50  0001 C CNN
	1    3800 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 60781685
P 4400 2100
F 0 "#PWR0103" H 4400 1850 50  0001 C CNN
F 1 "GND" H 4405 1927 50  0000 C CNN
F 2 "" H 4400 2100 50  0001 C CNN
F 3 "" H 4400 2100 50  0001 C CNN
	1    4400 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 607817BC
P 5750 3500
F 0 "#PWR0104" H 5750 3250 50  0001 C CNN
F 1 "GND" H 5755 3327 50  0000 C CNN
F 2 "" H 5750 3500 50  0001 C CNN
F 3 "" H 5750 3500 50  0001 C CNN
	1    5750 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 607818F3
P 6200 3500
F 0 "#PWR0105" H 6200 3250 50  0001 C CNN
F 1 "GND" H 6205 3327 50  0000 C CNN
F 2 "" H 6200 3500 50  0001 C CNN
F 3 "" H 6200 3500 50  0001 C CNN
	1    6200 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 6078426A
P 7750 2250
F 0 "#PWR0106" H 7750 2000 50  0001 C CNN
F 1 "GND" H 7755 2077 50  0000 C CNN
F 2 "" H 7750 2250 50  0001 C CNN
F 3 "" H 7750 2250 50  0001 C CNN
	1    7750 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 60784746
P 10450 3500
F 0 "#PWR0110" H 10450 3250 50  0001 C CNN
F 1 "GND" H 10455 3327 50  0000 C CNN
F 2 "" H 10450 3500 50  0001 C CNN
F 3 "" H 10450 3500 50  0001 C CNN
	1    10450 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 607849B4
P 12500 3500
F 0 "#PWR0112" H 12500 3250 50  0001 C CNN
F 1 "GND" H 12505 3327 50  0000 C CNN
F 2 "" H 12500 3500 50  0001 C CNN
F 3 "" H 12500 3500 50  0001 C CNN
	1    12500 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 60786AE7
P 14900 2850
F 0 "#PWR0113" H 14900 2600 50  0001 C CNN
F 1 "GND" H 14905 2677 50  0000 C CNN
F 2 "" H 14900 2850 50  0001 C CNN
F 3 "" H 14900 2850 50  0001 C CNN
	1    14900 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	14450 2850 14900 2850
$Comp
L power:GND #PWR0114
U 1 1 607CB4CF
P 15850 3000
F 0 "#PWR0114" H 15850 2750 50  0001 C CNN
F 1 "GND" H 15855 2827 50  0000 C CNN
F 2 "" H 15850 3000 50  0001 C CNN
F 3 "" H 15850 3000 50  0001 C CNN
	1    15850 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 607CB606
P 18000 3500
F 0 "#PWR0115" H 18000 3250 50  0001 C CNN
F 1 "GND" H 18005 3327 50  0000 C CNN
F 2 "" H 18000 3500 50  0001 C CNN
F 3 "" H 18000 3500 50  0001 C CNN
	1    18000 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 607CD511
P 21600 3800
F 0 "#PWR0116" H 21600 3550 50  0001 C CNN
F 1 "GND" H 21605 3627 50  0000 C CNN
F 2 "" H 21600 3800 50  0001 C CNN
F 3 "" H 21600 3800 50  0001 C CNN
	1    21600 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 607CDB55
P 20250 7100
F 0 "#PWR0117" H 20250 6850 50  0001 C CNN
F 1 "GND" H 20255 6927 50  0000 C CNN
F 2 "" H 20250 7100 50  0001 C CNN
F 3 "" H 20250 7100 50  0001 C CNN
	1    20250 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 607CDC8C
P 19900 8300
F 0 "#PWR0118" H 19900 8050 50  0001 C CNN
F 1 "GND" H 19905 8127 50  0000 C CNN
F 2 "" H 19900 8300 50  0001 C CNN
F 3 "" H 19900 8300 50  0001 C CNN
	1    19900 8300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 607CDDC3
P 19700 8300
F 0 "#PWR0119" H 19700 8050 50  0001 C CNN
F 1 "GND" H 19705 8127 50  0000 C CNN
F 2 "" H 19700 8300 50  0001 C CNN
F 3 "" H 19700 8300 50  0001 C CNN
	1    19700 8300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 607CDEFA
P 19250 8350
F 0 "#PWR0120" H 19250 8100 50  0001 C CNN
F 1 "GND" H 19255 8177 50  0000 C CNN
F 2 "" H 19250 8350 50  0001 C CNN
F 3 "" H 19250 8350 50  0001 C CNN
	1    19250 8350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 607D0871
P 14650 6850
F 0 "#PWR0121" H 14650 6600 50  0001 C CNN
F 1 "GND" H 14655 6677 50  0000 C CNN
F 2 "" H 14650 6850 50  0001 C CNN
F 3 "" H 14650 6850 50  0001 C CNN
	1    14650 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 607D09A8
P 14300 8050
F 0 "#PWR0122" H 14300 7800 50  0001 C CNN
F 1 "GND" H 14305 7877 50  0000 C CNN
F 2 "" H 14300 8050 50  0001 C CNN
F 3 "" H 14300 8050 50  0001 C CNN
	1    14300 8050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 607D0ADF
P 14100 8050
F 0 "#PWR0123" H 14100 7800 50  0001 C CNN
F 1 "GND" H 14105 7877 50  0000 C CNN
F 2 "" H 14100 8050 50  0001 C CNN
F 3 "" H 14100 8050 50  0001 C CNN
	1    14100 8050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 607D0C16
P 13650 8100
F 0 "#PWR0124" H 13650 7850 50  0001 C CNN
F 1 "GND" H 13655 7927 50  0000 C CNN
F 2 "" H 13650 8100 50  0001 C CNN
F 3 "" H 13650 8100 50  0001 C CNN
	1    13650 8100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 6081C388
P 9400 7250
F 0 "#PWR0125" H 9400 7000 50  0001 C CNN
F 1 "GND" H 9405 7077 50  0000 C CNN
F 2 "" H 9400 7250 50  0001 C CNN
F 3 "" H 9400 7250 50  0001 C CNN
	1    9400 7250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0126
U 1 1 6081C4BF
P 9050 8450
F 0 "#PWR0126" H 9050 8200 50  0001 C CNN
F 1 "GND" H 9055 8277 50  0000 C CNN
F 2 "" H 9050 8450 50  0001 C CNN
F 3 "" H 9050 8450 50  0001 C CNN
	1    9050 8450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0127
U 1 1 6081C5F6
P 8850 8450
F 0 "#PWR0127" H 8850 8200 50  0001 C CNN
F 1 "GND" H 8855 8277 50  0000 C CNN
F 2 "" H 8850 8450 50  0001 C CNN
F 3 "" H 8850 8450 50  0001 C CNN
	1    8850 8450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0128
U 1 1 6081C72D
P 8400 8500
F 0 "#PWR0128" H 8400 8250 50  0001 C CNN
F 1 "GND" H 8405 8327 50  0000 C CNN
F 2 "" H 8400 8500 50  0001 C CNN
F 3 "" H 8400 8500 50  0001 C CNN
	1    8400 8500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 6081DDF4
P 4050 7100
F 0 "#PWR0129" H 4050 6850 50  0001 C CNN
F 1 "GND" H 4055 6927 50  0000 C CNN
F 2 "" H 4050 7100 50  0001 C CNN
F 3 "" H 4050 7100 50  0001 C CNN
	1    4050 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0130
U 1 1 6081DF2B
P 3700 8300
F 0 "#PWR0130" H 3700 8050 50  0001 C CNN
F 1 "GND" H 3705 8127 50  0000 C CNN
F 2 "" H 3700 8300 50  0001 C CNN
F 3 "" H 3700 8300 50  0001 C CNN
	1    3700 8300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0131
U 1 1 6081E062
P 3500 8300
F 0 "#PWR0131" H 3500 8050 50  0001 C CNN
F 1 "GND" H 3505 8127 50  0000 C CNN
F 2 "" H 3500 8300 50  0001 C CNN
F 3 "" H 3500 8300 50  0001 C CNN
	1    3500 8300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0132
U 1 1 6081E199
P 3050 8350
F 0 "#PWR0132" H 3050 8100 50  0001 C CNN
F 1 "GND" H 3055 8177 50  0000 C CNN
F 2 "" H 3050 8350 50  0001 C CNN
F 3 "" H 3050 8350 50  0001 C CNN
	1    3050 8350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0133
U 1 1 6081E4F8
P 1750 12400
F 0 "#PWR0133" H 1750 12150 50  0001 C CNN
F 1 "GND" H 1755 12227 50  0000 C CNN
F 2 "" H 1750 12400 50  0001 C CNN
F 3 "" H 1750 12400 50  0001 C CNN
	1    1750 12400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0134
U 1 1 6081E62F
P 3500 12600
F 0 "#PWR0134" H 3500 12350 50  0001 C CNN
F 1 "GND" H 3505 12427 50  0000 C CNN
F 2 "" H 3500 12600 50  0001 C CNN
F 3 "" H 3500 12600 50  0001 C CNN
	1    3500 12600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0135
U 1 1 6081E766
P 3500 11400
F 0 "#PWR0135" H 3500 11150 50  0001 C CNN
F 1 "GND" H 3505 11227 50  0000 C CNN
F 2 "" H 3500 11400 50  0001 C CNN
F 3 "" H 3500 11400 50  0001 C CNN
	1    3500 11400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0136
U 1 1 6081E89D
P 3500 13800
F 0 "#PWR0136" H 3500 13550 50  0001 C CNN
F 1 "GND" H 3505 13627 50  0000 C CNN
F 2 "" H 3500 13800 50  0001 C CNN
F 3 "" H 3500 13800 50  0001 C CNN
	1    3500 13800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0137
U 1 1 60820860
P 10400 11200
F 0 "#PWR0137" H 10400 10950 50  0001 C CNN
F 1 "GND" H 10405 11027 50  0000 C CNN
F 2 "" H 10400 11200 50  0001 C CNN
F 3 "" H 10400 11200 50  0001 C CNN
	1    10400 11200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0138
U 1 1 60820997
P 12500 14750
F 0 "#PWR0138" H 12500 14500 50  0001 C CNN
F 1 "GND" H 12505 14577 50  0000 C CNN
F 2 "" H 12500 14750 50  0001 C CNN
F 3 "" H 12500 14750 50  0001 C CNN
	1    12500 14750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0139
U 1 1 60820ACE
P 7350 14500
F 0 "#PWR0139" H 7350 14250 50  0001 C CNN
F 1 "GND" H 7355 14327 50  0000 C CNN
F 2 "" H 7350 14500 50  0001 C CNN
F 3 "" H 7350 14500 50  0001 C CNN
	1    7350 14500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0140
U 1 1 60820C05
P 8800 14500
F 0 "#PWR0140" H 8800 14250 50  0001 C CNN
F 1 "GND" H 8805 14327 50  0000 C CNN
F 2 "" H 8800 14500 50  0001 C CNN
F 3 "" H 8800 14500 50  0001 C CNN
	1    8800 14500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0141
U 1 1 60820D3C
P 9850 14500
F 0 "#PWR0141" H 9850 14250 50  0001 C CNN
F 1 "GND" H 9855 14327 50  0000 C CNN
F 2 "" H 9850 14500 50  0001 C CNN
F 3 "" H 9850 14500 50  0001 C CNN
	1    9850 14500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0142
U 1 1 6082570B
P 20300 11150
F 0 "#PWR0142" H 20300 10900 50  0001 C CNN
F 1 "GND" H 20305 10977 50  0000 C CNN
F 2 "" H 20300 11150 50  0001 C CNN
F 3 "" H 20300 11150 50  0001 C CNN
	1    20300 11150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0143
U 1 1 60825842
P 19950 12350
F 0 "#PWR0143" H 19950 12100 50  0001 C CNN
F 1 "GND" H 19955 12177 50  0000 C CNN
F 2 "" H 19950 12350 50  0001 C CNN
F 3 "" H 19950 12350 50  0001 C CNN
	1    19950 12350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0144
U 1 1 60825979
P 19750 12350
F 0 "#PWR0144" H 19750 12100 50  0001 C CNN
F 1 "GND" H 19755 12177 50  0000 C CNN
F 2 "" H 19750 12350 50  0001 C CNN
F 3 "" H 19750 12350 50  0001 C CNN
	1    19750 12350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0145
U 1 1 60825AB0
P 19300 12400
F 0 "#PWR0145" H 19300 12150 50  0001 C CNN
F 1 "GND" H 19305 12227 50  0000 C CNN
F 2 "" H 19300 12400 50  0001 C CNN
F 3 "" H 19300 12400 50  0001 C CNN
	1    19300 12400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0146
U 1 1 6082BF73
P 1900 15350
F 0 "#PWR0146" H 1900 15100 50  0001 C CNN
F 1 "GND" H 1905 15177 50  0000 C CNN
F 2 "" H 1900 15350 50  0001 C CNN
F 3 "" H 1900 15350 50  0001 C CNN
	1    1900 15350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0147
U 1 1 6082CBD3
P 8150 12950
F 0 "#PWR0147" H 8150 12700 50  0001 C CNN
F 1 "GND" H 8155 12777 50  0000 C CNN
F 2 "" H 8150 12950 50  0001 C CNN
F 3 "" H 8150 12950 50  0001 C CNN
	1    8150 12950
	1    0    0    -1  
$EndComp
Connection ~ 8150 12950
Wire Wire Line
	10450 1750 10450 2300
Connection ~ 10450 2300
Wire Wire Line
	10450 2300 10450 2800
Connection ~ 10450 2800
Wire Wire Line
	10450 2800 10450 3450
Connection ~ 10450 3450
Wire Wire Line
	10400 3350 10400 2700
Connection ~ 10400 2700
Wire Wire Line
	10400 2700 10400 2200
Connection ~ 10400 2200
Wire Wire Line
	10400 2200 10400 1650
Connection ~ 10400 1650
Wire Wire Line
	10400 1650 10100 1650
Wire Wire Line
	12500 3500 12500 3250
Connection ~ 12500 3250
Wire Wire Line
	12500 3250 12500 2200
Wire Wire Line
	12450 3150 12450 2100
Connection ~ 12450 2100
Wire Wire Line
	12450 2100 12250 2100
Text Notes 8950 15850 0    252  ~ 50
Thermistors
$EndSCHEMATC
